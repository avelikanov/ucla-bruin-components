# :school_satchel: :nut_and_bolt: UCLA Web Components Library

Welcome to the UCLA Web Component Library.

The digital application of UCLA’s brand can be found here. We’ve developed a library of components, along with thoughtfully articulated guidelines, documentation, and code to assist campus partners in designing and developing robust and accessible experiences across all UCLA websites and applications.

Our goal is to provide flexible resources and tools for any person or team who contributes to UCLA’s diverse and substantial digital infrastructure.

**Project Built with:** [Fractal Component Library](https://fractal.build/)

**Hosting With:** AWS S3 + CloudFront CDN

**Maintained By:** Strategic Communications

---

### :pencil: For Consumers
- [How To Navigate the Website](./docs/consumers/navigatingSite.md)
- [Component Design, Code Documentation and Example Usage (Website)](https://webcomponents.ucla.edu/)
- [How To Include Library in Your Web Project (Website)](https://webcomponents.ucla.edu/build/1.0.0-beta.7/docs/installation/download.html)
- [Components Status](./docs/consumers/componentStatus.md)

---

### :computer: For Development Contributors
- [Get Local Environment Setup](./docs/contributors/getSetup.md)
- [Learn How To Contribute](./docs/contributors/howToContribute.md)
- [Project Directory Hierarchy](./docs/contributors/projectHierarchy.md)
- [Documentation On How to Build With Fractal](https://fractal.build/guide/documentation/)
- [Using BEM Naming Convention](./docs/contributors/namingConvention.md)

---

#### Have questions on how to consume or contribute to this library? Please reach out to one of our developers:
[ Report an Issue](https://bitbucket.org/uclaucomm/ucla-bruin-components/issues?status=new&status=open)
[Join the Slack Discussion](https://ucla.slack.com/archives/G01KJ3GJKHS)
Internal Project Maintainers, please see documentation [here](./docs/internal/tableofcontents.md).
