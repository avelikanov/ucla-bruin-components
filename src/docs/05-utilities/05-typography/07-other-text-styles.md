---
title: Other Text Styles
---
<a class="create-button small" href="https://bitbucket.org/uclaucomm/ucla-bruin-components/issues?status=new&status=open">![bitbucket](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/bitbucket-icon-white.png) Report an Issue</a>
<a class="create-button small" href="https://ucla.slack.com/archives/C01TW0HVB0Q">![Slack](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/slack-icon-white.png) Join the Slack Discussion</a>

{{view '@typography--right-align'}}
```
{{view '@typography--right-align'}}
```

{{view '@typography--left-align'}}
```
{{view '@typography--left-align'}}
```

{{view '@typography--center-align'}}
```
{{view '@typography--center-align'}}
```

{{view '@typography--strong'}}
```
{{view '@typography--strong'}}
```

{{view '@typography--emphasis'}}
```
{{view '@typography--emphasis'}}
```

{{view '@typography--deleted'}}
```
{{view '@typography--deleted'}}
```

{{view '@typography--marked'}}
```
{{view '@typography--marked'}}
```

{{view '@typography--inserted'}}
```
{{view '@typography--inserted'}}
```

Note: The following blockquote's styling does not behave as it does when being used. To see an better example, please see the component example [here](/build/%!CurrentVersion%!/components/detail/typography.html).

{{view '@typography--blockquote'}}

```
{{view '@typography--blockquote'}}
```

<p style="background: #2774AE;color: #fff;">The utility class `.has-background-ucla-blue` has been added so only the row has a modified background color. A second utility class `.has-white-text` has been added to turn the text white.</p>

```
<div class="ucla campus has-background-ucla-blue has-white-text">
  <div class="col span_1_of_1">
    <p>The utility class `.has-background-ucla-blue` has been added so only the row has a modified background color. A second utility class `.has-white-text` has been added to turn the text white.</p>
  </div>
</div>

```
