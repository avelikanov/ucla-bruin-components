---
title: Typography
---
<a class="create-button small" href="https://bitbucket.org/uclaucomm/ucla-bruin-components/issues?status=new&status=open">![bitbucket](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/bitbucket-icon-white.png) Report an Issue</a>
<a class="create-button small" href="https://ucla.slack.com/archives/C01TW0HVB0Q">![Slack](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/slack-icon-white.png) Join the Slack Discussion</a>

In this section we cover fonts, headings, rules, and type related use.

The UCLA official type is Helvetica. More on the brand type [here](http://brand.ucla.edu/identity/typography).

Helvetica and Arial are the primary font for online work. Helvetica and Arial are required for all UCLA-branded websites. A special branded version of Helvetica is used only for the [department logo system](http://brand.ucla.edu/identity/logos-and-marks).

## Web Considerations
* Remember that the browser, not the designer, controls the display of type. That’s why Helvetica/Arial is a good choice — these neutral type styles don’t require adjustments like extra line spacing.
* Headlines should always convey the content hierarchy: think of your headlines and subheads as a content outline.
* Use short copy and bulleted lists rather than big blocks of text. This is easier for all readers to scan, and it improves accessibility for users who depend on screen readers.
