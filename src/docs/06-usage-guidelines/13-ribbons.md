---
title: Ribbons
---
{{view '@support'}}

Highlight important ideas or brand tag lines.

## **Usage**

### **Usability Guidance**

* Limit 1-2 ribbons per page and focus on your most important message.

### **Accessibility Requirements**

* Use the approved brand colors to ensure ribbons have high color contrast and meet accessibility standards.
* Use the `<aside>` element to denote ribbons as standalone content.

## **Brand Ribbon**

### **Code**

```
{{ view '@ribbons--brand' }}
```

### **Code**

```
{{ view '@ribbons--highlight' }}
```
