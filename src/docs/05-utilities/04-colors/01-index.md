---
title: Colors
---
<a class="create-button small" href="https://bitbucket.org/uclaucomm/ucla-bruin-components/issues?status=new&status=open">![bitbucket](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/bitbucket-icon-white.png) Report an Issue</a>
<a class="create-button small" href="https://ucla.slack.com/archives/C01TW0HVB0Q">![Slack](https://s3.us-west-1.amazonaws.com/webcomponents.ucla.edu/build/%!CurrentVersion%!/docs/img/slack-icon-white.png) Join the Slack Discussion</a>

UCLA official colors communicate our brand identity and are the building blocks of accessibility. Use of these colors are a required brand element. Learn more about UCLA brand colors on the [UCLA Brand Guidelines page](http://brand.ucla.edu/identity/colors).

We define the following variables in a [Sass](https://sass-lang.com/) stylesheet located at `./src/scss/utilities/_colors.scss`. Use Sass to compile it back to CSS.

> **NOTE**: ADA compliance requires a contrast ratio of at least 4.5:1 for normal text and 3:1 for large text, and a contrast ratio of at least 3:1 for graphics and user interface components (such as form input borders). Large text is defined as 14 point (typically 18.66px) and bold or larger, or 18 point (typically 24px) or larger.

- [Layout Colors](./colors/layout.html)
- [Text Colors](./colors/text.html)
- [Interactive Colors - Light Background](./colors/interactive-light.html)
- [Interactive Colors - Dark Background](./colors/interactive-dark.html)
- [Denotive Colors](./colors/denotive.html)

**For more information about contributing to Colors, see README file at `./src/components/98-colors/README.md`**
