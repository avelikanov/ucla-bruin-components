# [1.0.0-beta.16](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-beta.15...v1.0.0-beta.16) (2021-09-28)


### Bug Fixes

* bug fix ([27d2017](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/27d2017ab5367823e94303e9719eb8ffc640613e))

# [1.0.0-beta.15](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-beta.14...v1.0.0-beta.15) (2021-07-20)


### Bug Fixes

* new button, social tiles reconfig, doc and readme updates ([17c189a](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/17c189a9b9b217a69d92fee46b30a0f45baf9605))


### BREAKING CHANGES

* Social tiles have been reconfigured

58, 62

<<<<<<< HEAD
# [1.0.0-beta.14](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-beta.13...v1.0.0-beta.14) (2021-06-08)


### Features

* **component:** cards and spacing ([5baaebb](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/5baaebb3fee99f6932ba766b0442dbd020772d71))
=======
# [1.0.0-dev-beta.17](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.16...v1.0.0-dev-beta.17) (2021-07-20)


### Bug Fixes

* new button, social tiles reconfig, doc and readme updates ([17c189a](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/17c189a9b9b217a69d92fee46b30a0f45baf9605))


### BREAKING CHANGES

* Social tiles have been reconfigured

58, 62
>>>>>>> 98f2a12a3a3c1d7cacd8525d3a785bb93383a247

# [1.0.0-dev-beta.16](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.15...v1.0.0-dev-beta.16) (2021-06-08)


### Features

* **component:** cards and spacing ([5baaebb](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/5baaebb3fee99f6932ba766b0442dbd020772d71))

# [1.0.0-dev-beta.15](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.14...v1.0.0-dev-beta.15) (2021-05-24)


### Bug Fixes

* **component:** update cards and svg ([44d48ab](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/44d48ab787ce58713eb36006460a5a669d017b58))

# [1.0.0-dev-beta.14](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.13...v1.0.0-dev-beta.14) (2021-04-15)


### Bug Fixes

* **tab.js:** console Error Fix ([6062280](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/60622808ebc54c3bebc16d3cff64ab2e75247fcb))

# [1.0.0-dev-beta.13](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.12...v1.0.0-dev-beta.13) (2021-04-15)


### Bug Fixes

* **tab.js:** fix CDN Javascript Error ([93bba5b](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/93bba5be1c38b9638387d3b7ebe105b60964c4eb))

# [1.0.0-dev-beta.12](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.11...v1.0.0-dev-beta.12) (2021-04-14)


### Bug Fixes

* **grid, typography:** fix spacing ([b6b56dc](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/b6b56dc3cae624fc70deb83c1b3b3b1718c4d6c9))

# [1.0.0-dev-beta.11](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.10...v1.0.0-dev-beta.11) (2021-04-06)


### Features

* **accordion, primary navigation:** added Accordion and Primary Navigation Component ([3bf151d](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/3bf151dcaacf7b4fab1682e97b26bf7590e2823d)), closes [#6](http://bitbucket.org/uclaucomm/ucla-bruin-components/issue/6) [#8](http://bitbucket.org/uclaucomm/ucla-bruin-components/issue/8)
* **primary navigation, grid:** primary Nav Escape and Grid Spacing ([60e36bd](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/60e36bd680ff6def1b14118b9d09632176527a3b))

# [1.0.0-dev-beta.10](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.9...v1.0.0-dev-beta.10) (2021-03-16)


### Features

* **footer and email:** including footer and email sign up component ([32fb5bf](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/32fb5bfc1b41184934bea37f7c3347ac0c7d67d2))

# [1.0.0-dev-beta.9](http://bitbucket.org/uclaucomm/ucla-bruin-components/compare/v1.0.0-dev-beta.8...v1.0.0-dev-beta.9) (2021-02-12)


### Features

* **tile component:** including tile component ([4853bc0](http://bitbucket.org/uclaucomm/ucla-bruin-components/commits/4853bc0d8a26f31f3305b567bae22393a75504a2))
